part of 'main.dart';

class InspectBeePage extends StatelessWidget {
  final String bee;

  InspectBeePage({Key key, @required this.bee}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Inspect Bee'),
          /*leading: IconButton(
          icon: Icon(Icons.arrow_back),
          // onPressed: _returnBeedex,
        ),*/
        ),
        body: Center(
          child: Text(bee),
        ));
  }
}
