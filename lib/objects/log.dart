class LogObject {
  String imagePath;
  String species;
  String datetime;
  bool online;

  LogObject(String imagePath, String species, String datetime, bool online) {
    this.imagePath = imagePath;
    this.species = species;
    this.datetime = datetime;
    this.online = online;
  }
}
