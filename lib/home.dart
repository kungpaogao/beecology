part of 'main.dart';

class HomePage extends StatelessWidget {
  Widget _buildBlock(String iconName, String title, String subtitle) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black38,
            offset: Offset(2.0, 2.0),
            blurRadius: 4.0,
          ),
        ],
        color: Colors.yellow[700],
      ),
      margin: EdgeInsets.only(
        top: 16.0,
      ),
      padding: EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(
                right: 16.0,
              ),
              child: Icon(
                IconData(
                  0xea1e,
                  fontFamily: 'MaterialOutline',
                ),
                size: 60.0,
                color: Colors.black,
              )
              /*
            child: Text(
              "ac_unit",
              style: TextStyle(
                fontFamily: "MaterialBase",
                fontSize: 12,
              ),
              //color: Colors.black,
              //size: 60.0,
            ),
            */
              ),
          Expanded(
            child: Column(
              //spacing: 2.0,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.grey[800],
                    height: 1.5,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          bottom: 16.0,
        ),
        children: <Widget>[
          _buildBlock(
            //Icons.camera_alt,
            "camera_alt",
            "Take a picture",
            "Take a picture using your phone's camera",
          ),
          _buildBlock(
            //Icons.videocam,
            "videocam",
            "Take a video",
            "Take a video using your phone's camera",
          ),
          _buildBlock(
            //Icons.photo,
            "photo",
            "Import a Picture",
            "Import a picture you took earlier",
          ),
          _buildBlock(
            //Icons.edit,
            "edit",
            "Log as Expert",
            "Directly record a bee",
          ),
        ],
      ),
    );
  }
}
