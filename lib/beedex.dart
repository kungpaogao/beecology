part of 'main.dart';

class BeedexPage extends StatelessWidget {
  Widget _buildRow(
      BuildContext context, String imagePath, String title, String subtitle) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => InspectBeePage(bee: title)));
      },
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          children: <Widget>[
            // "profile picture" of the bee species
            Container(
              margin: EdgeInsets.only(
                right: 16.0,
              ),
              width: 55.0,
              height: 55.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.lime,
                image: DecorationImage(
                  image: AssetImage(imagePath),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            // section for the species name and common name
            Expanded(
              child: Wrap(
                runSpacing: 5.0,
                children: <Widget>[
                  // species name
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 20.0,
                      // fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  // common name
                  Text(
                    subtitle,
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _createEntries(BuildContext context) {
    List<Widget> entries = [];
    for (BeeObject bee in Bumblebees.getBumblebees()) {
      entries
          .add(_buildRow(context, bee.imagePath, bee.commonName, bee.species));
    }
    return entries;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: _createEntries(context),
      ),
    );
  }
}
