import 'package:flutter/material.dart';
import 'objects/log.dart';
import 'objects/bee.dart';
part 'home.dart';
part 'beedex.dart';
part 'mylogs.dart';
part 'inspectbee.dart';

void main() {
  runApp(Beecology());
}

class Beecology extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Beecology',
      theme: new ThemeData(
        primaryColor: Colors.yellow[700],
        // TODO: change theme color to have the BottomNav icons black when not selected
      ),
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {
  // This widget is the root of your application.
  PageController _pageController;
  int _navIndex;

  // action to navigate between pages with BottomNavigation
  void _bottomNavTap(int page) {
    _pageController.animateToPage(
      page,
      duration: const Duration(milliseconds: 1), // no transition
      curve: Curves.ease,
    );
    setState(() {
      _navIndex = page;
    });
  }

  // set initial state: initialize pageController, set page index to 0 (home page)
  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
    _navIndex = 0;
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Beecology",
        ),
      ),
      */
      // allow changing through different pages
      body: SafeArea(
        child: PageView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            HomePage(),
            MyLogsPage(),
            BeedexPage(),
          ],
          controller: _pageController,
        ),
      ),
      // bottom navigation items w/ icons and text
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.bug_report,
            ),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text('My Logs'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            title: Text('Beedex'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
        ],
        onTap: _bottomNavTap,
        currentIndex: _navIndex,
      ),
    );
  }
}
