class BeeObject {
  String imagePath, commonName, species, desc;
  List<String> commonConfusions, activeMonths;

  BeeObject(String imagePath, String commonName, String species, String desc,
      List<String> commonConfusions, List<String> activeMonths) {
    this.imagePath = imagePath;
    this.species = species;
    this.commonName = commonName;
    this.desc = desc;
    this.commonConfusions = commonConfusions;
    this.activeMonths = activeMonths;
  }
}

class Bumblebees {
  static List<BeeObject> bumblebees = [
    BeeObject(
        "assets/bee.jpg",
        "Alpha Bumblebee",
        "Bombus alpha",
        "This bee hella alpha.",
        ["omega"],
        ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"]),
    BeeObject(
        "assets/bee.jpg",
        "Alpha Bumblebee",
        "Bombus alpha",
        "This bee hella alpha.",
        ["omega"],
        ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"]),
  ];

  static List getBumblebees() {
    return bumblebees;
  }
}
