part of 'main.dart';

class MyLogsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _MyLogsPageState();
  }
}

class _MyLogsPageState extends State<MyLogsPage> {
  List<LogObject> _logsList = [
    // datetime should be formatted: yyyy/mm/dd HH:mm
    LogObject("yo", "Bombus affinis", "2018/06/23 20:35", true),
    LogObject("yo", "Bombus bffinis", "2018/02/04 22:35", true),
    LogObject("yo", "Bombus dffinis", "2018/02/04 13:35", false),
    LogObject("yo", "Bombus zffinis", "2018/01/22 22:35", true),
    LogObject("yo", "Bombus gffinis", "2018/09/04 22:35", false),
    LogObject("yo", "Bombus zffinis", "2018/10/18 21:35", false),
  ];

  static const List<String> sortOptionList = <String>[
    'Alphabetically (A to Z)',
    'Alphabetically (Z to A)',
    'Time (new to old)',
    'Time (old to new)',
  ];

  void _sortLogs(String choice) {
    switch (choice) {
      case 'Alphabetically (A to Z)':
        print('alpha a z');
        setState(() {
          _logsList.sort((a, b) => a.species.compareTo(b.species));
        });
        break;
      case 'Alphabetically (Z to A)':
        print('alpha z a');
        setState(() {
          _logsList.sort((b, a) => a.species.compareTo(b.species));
        });
        break;
      case 'Time (new to old)':
        print('time new old');
        setState(() {
          _logsList.sort((b, a) => a.datetime.compareTo(b.datetime));
        });
        break;
      case 'Time (old to new)':
        print('time old new');
        setState(() {
          _logsList.sort((a, b) => a.datetime.compareTo(b.datetime));
        });
        break;
      default:
        print('default');
        setState(() {
          _logsList.sort((a, b) => a.datetime.compareTo(b.datetime));
        });
        break;
    }
  }

  Widget _buildLogs() {
    return ListView.builder(itemBuilder: (context, index) {
      if (index < _logsList.length) {
        return _buildRow(_logsList[index]);
      }
    });
  }

  Widget _buildRow(LogObject log) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          // TODO: make icon a picture of the picture from the log (or default to abbreviation if picture is unavailable)
          Container(
            margin: EdgeInsets.only(
              right: 16.0,
            ),
            width: 55.0,
            height: 55.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Theme.of(context).primaryColor,
              // image: INSERT IMAGE FROM LOG
            ),
            child: Center(
              child: Text(
                log.species.substring(0, 1) + log.species.substring(7, 8),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 30.0,
                ),
              ),
            ),
          ),
          Expanded(
            child: Wrap(
              runSpacing: 5.0,
              children: <Widget>[
                Text(
                  log.species,
                  style: TextStyle(
                    fontSize: 20.0,
                    // fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                Text(
                  log.datetime,
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.grey[600],
                  ),
                ),
              ],
            ),
          ),
          log.online
              ? Icon(
                  Icons.cloud,
                  size: 20.0,
                  color: Colors.grey[400],
                )
              : Container(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(16.0),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  offset: Offset(2.0, 2.0),
                  blurRadius: 4.0,
                ),
              ],
              color: Colors.grey[300],
            ),
            alignment: Alignment.centerRight,
            child: PopupMenuButton(
              //icon: Icon(Icons.sort),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(right: 8.0),
                    child: Text(
                      'Sort by',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Icon(
                    Icons.sort,
                  ),
                ],
              ),
              onSelected: _sortLogs,
              itemBuilder: (BuildContext context) {
                return sortOptionList.map((String choice) {
                  return PopupMenuItem<String>(
                    child: Text(choice),
                    value: choice,
                  );
                }).toList();
              },
            ),
          ),
          Expanded(
            child: _buildLogs(),
          )
        ],
      ),
    );
  }
}
